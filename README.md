# MPL Video Player

The best video player out there. Is your video 30FPS? Well, we can probably do 10FPS at most right now. Does it have
audio ? Well, we can't have audio. Yet.

Built on top of [media-processing-lib](https://gitlab.com/meehai/media-processing-lib).

Usage:

```
./mpl_video_player.py /path/to/video
```
