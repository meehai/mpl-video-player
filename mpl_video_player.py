#!/usr/bin/env python3
"""the little matplotlib figure that could"""
from vre.utils import FFmpegVideo
from loggez import make_logger
import tkinter as tk
from PIL import Image, ImageTk
import time
from queue import Queue
import threading
import sys

logger = make_logger("MPL_VIDEO_PLAYER")

class VideoWrapper(FFmpegVideo):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fps_multiplier: float = 1.0
        self._current_frame: int = 0
        self.is_paused: bool = False

    @property
    def current_frame(self):
        return self._current_frame

    @current_frame.setter
    def current_frame(self, current_frame: int):
        if not 0 <= current_frame < len(self):
            logger.info(f"Frame {current_frame} wanted which is wrong. Reset to 0")
            current_frame = 0
        self._current_frame = int(current_frame)

class Window(threading.Thread):
    """with the help of the chat-ge-pe-tees"""
    def __init__(self, queue: Queue, video: VideoWrapper):
        super().__init__()
        self.queue = queue
        self.video = video

    def on_key_press(self, e: tk.Event):
        if e.char == "-":
            self.video.fps_multiplier = max(self.video.fps_multiplier / 2, 0.25)
            logger.info(f"Speed: {self.video.fps_multiplier}x")
        if e.char == "=":
            self.video.fps_multiplier = min(self.video.fps_multiplier * 2, 4.0)
            logger.info(f"Speed: {self.video.fps_multiplier}x")
        if e.keycode == 9: # esc
            self.root.destroy()
        if e.keycode == 65: # space
            self.video.is_paused = not self.video.is_paused
        if e.keycode == 113: # left arrow -> jump half a second back
            logger.debug((self.video.current_frame, round(self.video.current_frame - self.video.fps // 3)))
            self.video.current_frame = self.video.current_frame - self.video.fps // 3
        if e.keycode == 114: # right arrow -> jump half a second forward
            logger.debug((self.video.current_frame, round(self.video.current_frame + self.video.fps // 3)))
            self.video.current_frame = self.video.current_frame + self.video.fps // 3

    def run(self):
        self.root = tk.Tk()
        self.root.title(self.video.path.name)
        self.canvas = tk.Canvas(self.root, width=self.video.frame_shape[1], height=self.video.frame_shape[0])
        self.canvas.pack()
        self.canvas.bind("<KeyPress>", self.on_key_press)
        self.canvas.focus_set()

        while True:
            self.root.update()
            img_arr = self.queue.get(block=True)
            photo = ImageTk.PhotoImage(image=Image.fromarray(img_arr))
            self.canvas.create_image(0, 0, anchor=tk.NW, image=photo)

def read_loop(video: VideoWrapper, q: Queue):
    """simply reads the current frame as given by video.current_frame. This can be updated by the gui thread"""
    while True:
        try:
            q.put(video[video.current_frame], timeout=0.5)
        except Exception as e:
            logger.info(f"Program closed: {e}")
            exit(0)

        next_frame = video.current_frame if video.is_paused else (video.current_frame + 1) % len(video)
        video.current_frame = next_frame
        time.sleep(1 / (video.fps * video.fps_multiplier))

def main():
    q = Queue(maxsize=1)
    video = VideoWrapper(sys.argv[1])
    # just start the thread and don't think about join.
    Window(q, video).start()
    read_loop(video, q)

if __name__ == "__main__":
    main()
